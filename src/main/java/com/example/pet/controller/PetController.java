package com.example.pet.controller;

import com.example.pet.model.PetDTO;
import com.example.pet.model.PetUpdateRequest;
import com.example.pet.service.PetService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pet")
@RequiredArgsConstructor
public class PetController {

    private final PetService petService;

    @GetMapping("/allPetsList")
    public ResponseEntity<List<PetDTO>> getAllDataFromDatabase() {

        return ResponseEntity.ok(petService.listAllPets());
    }


    @GetMapping("/search/age/{age_from}/{age_to}")
    public List<PetDTO> getFromDatabaseByAge(@PathVariable(name = "age_from") Integer ageFrom,
                                             @PathVariable(name = "age_to") Integer ageTo){
        return petService.searchByAge(ageFrom, ageTo);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<Void> updatePet(@RequestBody PetUpdateRequest updateRequest){
        boolean result = petService.update(updateRequest);

        if(result){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build(); // 400 = bad request = wina błędu zapytania = użytkownika
        }
    }

    @PutMapping("/put")
    public ResponseEntity<Void> putIntoDatabase(@RequestBody PetDTO obiekt){
        boolean result = petService.saveOrUpdate(obiekt);

        if(result){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build(); // 400 = bad request = wina błędu zapytania = użytkownika
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deletePet(@PathVariable(name="id") Long identifier){
        boolean result = petService.delete(identifier);
        if(result){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.badRequest().build(); // 400 = bad request = wina błędu zapytania = użytkownika
        }
    }


}
