package com.example.pet.repository;


import com.example.pet.model.Pet;
import com.example.pet.model.PetDTO;
import com.example.pet.model.Race;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PetRepository extends JpaRepository<Pet, Long> {

    List<Pet> findAllByOwnerName(String name); // metoda zostanie wygenerowana przez spring
    List<Pet> findAllByRace(Race race); // metoda zostanie wygenerowana przez spring
    List<Pet> findAllByAgeBetween(Integer from, Integer to);
   // List<PetDTO> findTopByWeightAndWeightBefore(Double maxWeight);


}
