package com.example.pet.service;

import com.example.pet.model.Pet;
import com.example.pet.model.PetDTO;
import com.example.pet.model.PetUpdateRequest;
import com.example.pet.model.mapper.PetMapper;
import com.example.pet.repository.PetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PetService {
    private final PetRepository petRepository;
    private final PetMapper petMapper;

    public boolean saveOrUpdate(PetDTO petDto) {
        if (petDto.getPetsName() == null || petDto.getPetsName().isEmpty()) {
            return false; // nie udało się zapisać
        }
        Pet pet = petMapper.mapDtoToPet(petDto);
        petRepository.save(pet);
        return true;
    }

    public List<PetDTO> listAllPets() {
        // select * from pets...
        return petRepository.findAll()
                .stream()
                .map(petMapper::mapPetToDTO)
                .collect(Collectors.toList());
    }

    public List<PetDTO> searchByAge(Integer ageFrom, Integer ageTo) {
        return petRepository.findAllByAgeBetween(ageFrom, ageTo)
                .stream()
                .map(petMapper::mapPetToDTO)
                .collect(Collectors.toList());
    }

/*    public List<Pet> searchByWeight(Double maxWeight) {
        return petRepository.findTopByWeightAndWeightBefore(maxWeight)
                .stream()
                .map(petMapper::mapDtoToPet)
                .collect(Collectors.toList());
    }*/

    public boolean update(PetUpdateRequest updateRequest) {
        // select * from students where id = ?
        Optional<Pet> petOptional = petRepository.findById(updateRequest.getPetId());
        if (petOptional.isPresent()) { // jeśli udało się znaleźć rekord
            Pet pet = petOptional.get();

            if (updateRequest.getNewPetName() != null) {
                pet.setName(updateRequest.getNewPetName());
            }
            if (updateRequest.getNewPetAge() != null) {
                pet.setAge(updateRequest.getNewPetAge());
            }
            if (updateRequest.getNewPetOwnerName() != null) {
                pet.setOwnerName(updateRequest.getNewPetOwnerName());
            }
            if (updateRequest.getNewPetRace() != null) {
                pet.setRace(updateRequest.getNewPetRace());
            }

            petRepository.save(pet); // finalne zapisanie obiektu
            return true;
        }
        return false;
    }

    public boolean delete(Long identifier) {
        petRepository.deleteById(identifier);
        return true;
    }
}
