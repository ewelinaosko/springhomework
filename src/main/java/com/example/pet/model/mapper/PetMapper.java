package com.example.pet.model.mapper;

import com.example.pet.model.Pet;
import com.example.pet.model.PetDTO;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

// @Component == @Bean
//
@Mapper(componentModel = "spring")
public interface PetMapper {
    @Mappings({
            @Mapping(target = "name", source = "petsName"),
            @Mapping(target = "age", source = "petsAge"),
            @Mapping(target = "race", source = "race"),
            @Mapping(target = "pureRace", ignore = true),
            @Mapping(target = "ownerName", ignore = true),
            @Mapping(target = "weight", ignore = true),
            @Mapping(target = "id", ignore = true)
    })
    Pet mapDtoToPet(PetDTO dto);


    @Mappings({
            @Mapping(source  = "name", target = "petsName"),
            @Mapping(source  = "age", target = "petsAge"),
            @Mapping(source  = "race", target = "race"),
            @Mapping(source  = "pureRace", target = "pureRace"),
            @Mapping(source  = "ownerName", target = "ownerName"),
            @Mapping(source  = "weight", target = "weight")
    })
    PetDTO mapPetToDTO(Pet dto);
}
