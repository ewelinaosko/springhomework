package com.example.pet.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PetUpdateRequest {
    private Long petId;
    private String newPetName;
    private String newPetOwnerName;
    private Integer newPetAge;
    private Race newPetRace;
}
