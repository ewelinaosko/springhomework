package com.example.pet.model;

public enum Race {

    LABRADOR("labrador"),
    HUSKY("Husky"),
    GOLDEN_RETRIEVER("Golden retriever"),
    MOPS("Mops"),
    JAMNIK("Jamnik"),
    CHIUHUAHUA("chiuhuahua");

    public final String label;

    private Race(String label) {
        this.label = label;
    }
}
