package com.example.pet.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PetDTO {
    private String petsName;
    private Integer petsAge;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String ownerName;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Double weight;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Boolean pureRace;
    private Race race;
}
