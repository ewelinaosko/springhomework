package com.example.pet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetApplication.class, args);
    }

}
/*    Treść zadania domowego - 1 - REST:
        Stwórz aplikację której celem (analogicznie do treści realizowanej na zajęciach) jest stworzenie funkcjonalności zarządzania obiektem (CURD + wyszukiwanie specyficzne dla klasy Pet ) w bazie danych.
        Model: Klasa Pet
        name (imie),
        age (wiek),
        ownerName (imie właściciela),
        weight (waga [double])
        pureRace (czy czystej rasy [boolean])
        race (rasa - jedna z wartości enum poniżej)
        Enum Race:
        LABRADOR
        HUSKY
        GOLDEN_RETRIEVER
        MOPS
        JAMNIK
        CHIUHUAHUA
        PetDTO:
        petsName
        petsAge
        race

        Zapytania (RequestParam):
        wyszukiwanie po właścicielu (ma zwracać listę Pet a nie PetDTO)
        wyszukiwanie po rasie psa (ma zwracać listę PetDTO)

        Zapytania (Path Variable):
        wyszukiwanie po wieku z przedziału (wynik: List<PetDto>)
        wyszukiwanie z wagą maximum (podajemy max) (wynik: List<Pet>)

        Podstawowe funkcje:
        metoda GET - listuje wszystkie Pet (wynik to List<PetDTO)
        metoda PUT - dodaje Pet do bazy.
        metoda DELETE - usuwa Pet z bazy (przyjmuje identyfikator zwierzaka jako REQUEST PARAM)
        metoda POST - edycja obiektu Pet - podajemy UpdatePetRequest do edytowania parametrów:
        UpdatePetRequest :
        name
        owner
        age
        race*/
